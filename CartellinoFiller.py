from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import WebDriverException
from selenium.common.exceptions import TimeoutException

import sys
import datetime
import re
import itertools
import argparse

#print italian style dates
import locale
locale.setlocale(locale.LC_TIME, "it_IT")

class TimeStamp:
  def __init__(self,dtobj):
    self.timestamp=dtobj
  def selectSede(self,selectElement):
    selectSede=Select(selectElement)
    selectSede.select_by_visible_text('Sezione di Trieste')
    return
  def modifyTime(self,dateTimeBox):
    ##check date
    #print(dateTimeBox.get_attribute('value'))
    #print(self.timestamp.strftime('%d %b %Y').lower())
    if self.timestamp.strftime('%d %b %Y').lower() not in dateTimeBox.get_attribute('value'): raise Exception("Got the wrong box!") 
    dateTimeBox.send_keys(Keys.BACKSPACE*5)
    dateTimeBox.send_keys(self.timestamp.strftime("%H:%M"))
    return
  def enterData(self,driver):
    raise Exception("This method must be overloaded")

class PairTimeStamp(TimeStamp):
  def __init__(self,entryTS,exitTS):
    self.entryTimeStamp=entryTS
    self.exitTimeStamp=exitTS
  def __str__(self):
    return 'Pair:  \n  '+str(self.entryTimeStamp)+'\n  '+str(self.exitTimeStamp)
  def enterData(self,driver):
    #table=driver.find_element_by_class_name('iceDatTbl')
    #rows=table.find_elements_by_xpath('tbody/tr')
    #showFormsButton=rows[int(self.entryTimeStamp.timestamp.day)-1].find_element_by_xpath('td[18]/div/form/input[@class="iceCmdBtn"]')
    monthOfShowFormsButtons=driver.find_elements_by_xpath('//input[@title="Click per mostrare le operazioni disponibili"]')
    showFormsButton=monthOfShowFormsButtons[int(self.entryTimeStamp.timestamp.day)-1]
    #showFormsButton=driver.find_element_by_xpath('//*[@id="j_id148:'+str(int(self.entryTimeStamp.timestamp.day)-1)+':j_id325:j_id326"]')
    showFormsButton.click()
    wait = WebDriverWait(driver, 10)
    element = wait.until(EC.visibility_of_element_located((By.XPATH, '//form[@id="cartellinoformentrata"]')))
    ###entry bit
    entrataBox=driver.find_element_by_xpath('//form[@id="cartellinoformentrata"]')
    dateTimeBox=entrataBox.find_element_by_xpath('//input[@class="iceSelInpDateInput" and contains(@id,"cartellinoformentrata")]')
    self.entryTimeStamp.modifyTime(dateTimeBox)
    selectElement=entrataBox.find_element_by_xpath('//select[@class="iceSelOneMnu" and contains(@id,"cartellinoformentrata")]')
    wait = WebDriverWait(driver, 10)
    element = wait.until(EC.element_to_be_clickable((By.XPATH, '//select[@class="iceSelOneMnu" and contains(@id,"cartellinoformentrata")]')))
    self.selectSede(selectElement)
    ###exit bit
    uscitaBox=driver.find_element_by_xpath('//form[@id="cartellinoformuscita"]')
    dateTimeBox=uscitaBox.find_element_by_xpath('//input[@class="iceSelInpDateInput" and contains(@id,"cartellinoformuscita")]')
    self.exitTimeStamp.modifyTime(dateTimeBox)
    selectElement=uscitaBox.find_element_by_xpath('//select[@class="iceSelOneMnu" and contains(@id,"cartellinoformuscita")]')
    self.selectSede(selectElement)
    ###submit bit
    insertBothButton=uscitaBox.find_element_by_xpath('//input[@class="iceCmdBtn" and contains(@id,"cartellinoformuscita") and @value="Inserisci Entrata e Uscita"]')
    wait = WebDriverWait(driver, 10)
    element = wait.until(EC.element_to_be_clickable((By.XPATH, '//input[@class="iceCmdBtn" and contains(@id,"cartellinoformuscita") and @value="Inserisci Entrata e Uscita"]')))
    insertBothButton.click()
    return
  
class EntryTimeStamp(TimeStamp):
  def __init__(self,dtobj):
    super().__init__(dtobj)
  def __str__(self):
    return self.timestamp.strftime('E: %d/%m/%Y %H%M')
  def __repr__(self):
    return self.timestamp.strftime('E: %d/%m/%Y %H%M')
  def enterData(self,driver):
    table=driver.find_element_by_class_name('iceDatTbl')
    rows=table.find_elements_by_xpath('tbody/tr')
    showFormsButton=rows[int(self.timestamp.day)-1].find_element_by_xpath('td[18]/div/form/input[@class="iceCmdBtn"]')
    showFormsButton.click()
    entrataBox=driver.find_element_by_xpath('//form[@id="cartellinoformentrata"]')
    dateTimeBox=entrataBox.find_element_by_xpath('//input[@class="iceSelInpDateInput" and contains(@id,"cartellinoformentrata")]')
    self.modifyTime(dateTimeBox)
    selectElement=entrataBox.find_element_by_xpath('//select[@class="iceSelOneMnu" and contains(@id,"cartellinoformentrata")]')
    self.selectSede(selectElement)
    insertButton=entrataBox.find_element_by_xpath('//input[@class="iceCmdBtn" and contains(@id,"cartellinoformentrata")]')
    insertButton.click()
    return

class ExitTimeStamp(TimeStamp):
  def __init__(self,dtobj):
    super().__init__(dtobj)
  def __str__(self):
    return self.timestamp.strftime('U: %d/%m/%Y %H%M')
  def __repr__(self):
    return self.timestamp.strftime('U: %d/%m/%Y %H%M')
  def enterData(self,driver):
    table=driver.find_element_by_class_name('iceDatTbl')
    rows=table.find_elements_by_xpath('tbody/tr')
    showFormsButton=rows[int(self.timestamp.day)-1].find_element_by_xpath('td[18]/div/form/input[@class="iceCmdBtn"]')
    showFormsButton.click()
    uscitaBox=driver.find_element_by_xpath('//form[@id="cartellinoformuscita"]')
    dateTimeBox=uscitaBox.find_element_by_xpath('//input[@class="iceSelInpDateInput" and contains(@id,"cartellinoformuscita")]')
    self.modifyTime(dateTimeBox)
    selectElement=uscitaBox.find_element_by_xpath('//select[@class="iceSelOneMnu" and contains(@id,"cartellinoformuscita")]')
    self.selectSede(selectElement)
    insertButton=uscitaBox.find_element_by_xpath('//input[@class="iceCmdBtn" and contains(@id,"cartellinoformuscita")]')
    insertButton.click()
    return

class AttivitaFuoriSede:
  def __init__(self,startdt,enddt,text):
    self.startdt=startdt
    self.enddt=enddt
    self.text=text
  def __str__(self):
    return 'AFS: '+self.text+'\n  '+self.startdt.strftime('Start: %d/%m/%Y %H%M')+'\n  '+self.enddt.strftime('End: %d/%m/%Y %H%M')
  def modifyTime(self,dateTimeBox,dtobj):
    dateTimeBox.send_keys(Keys.BACKSPACE*17)
    dateTimeBox.send_keys(dtobj.strftime('%d %b %Y %H:%M').lower())
    return
  def sanityCheck(self):
    worktime=self.enddt-self.startdt
    return worktime > datetime.timedelta() and worktime < datetime.timedelta(hours=12)
  def enterData(self,driver):
    Select(driver.find_element_by_xpath('//*[@id="richiestagiustificativo:taskType"]')).select_by_visible_text('Attività fuori sede I-III')
    try:
      wait = WebDriverWait(driver, 5)
      element = wait.until(EC.visibility_of_element_located((By.XPATH, '//*[@id="richiestagiustificativo:choicemenu:_2"]')))
    except TimeoutException:
      Select(driver.find_element_by_xpath('//*[@id="richiestagiustificativo:taskType"]')).select_by_visible_text('')
      Select(driver.find_element_by_xpath('//*[@id="richiestagiustificativo:taskType"]')).select_by_visible_text('Attività fuori sede I-III')
      #Select(taskTypeSelectBox).select_by_visible_text('')
      #Select(taskTypeSelectBox).select_by_visible_text('Attività fuori sede I-III')
      wait = WebDriverWait(driver, 10)
      element = wait.until(EC.visibility_of_element_located((By.XPATH, '//*[@id="richiestagiustificativo:choicemenu:_2"]')))
    radioDalleOreAlleOre=driver.find_element_by_xpath('//*[@id="richiestagiustificativo:choicemenu:_2"]')
    radioDalleOreAlleOre.click()
    wait = WebDriverWait(driver, 10)
    element = wait.until(EC.visibility_of_element_located((By.XPATH, '//*[@id="richiestagiustificativo:daterefto"]')))
    startBox=driver.find_element_by_xpath('//*[@id="richiestagiustificativo:datereffrom"]')
    self.modifyTime(startBox,self.startdt)
    endBox=driver.find_element_by_xpath('//*[@id="richiestagiustificativo:daterefto"]')
    self.modifyTime(endBox,self.enddt)
    giustBox=driver.find_element_by_xpath('//*[@id="richiestagiustificativo:notarichiesta"]')
    giustBox.send_keys(self.text)
    submitButton=driver.find_element_by_xpath('//*[@id="richiestagiustificativo:j_id76"]')
    submitButton.click()
    wait = WebDriverWait(driver, 20)
    element = wait.until(EC.invisibility_of_element_located((By.XPATH, '//*[@id="richiestagiustificativo:daterefto"]')))
    return

def parseFile(filename):
  with open(filename,'r') as f:
    CSVlines=f.readlines()
  timeStamps=[]
  for line in CSVlines:
    cols=line.split(',')
    date=cols[26]
    times=cols[19]
    try:
      datetime.datetime.strptime(date,'%d/%m/%Y')
    except Exception: continue
    ###NOTE: untested on multiple entries/exits in a day
    entryTimeMatches=re.findall(r'E\d{4}',times)
    for et in entryTimeMatches: timeStamps.append(EntryTimeStamp(datetime.datetime.strptime(date+et,'%d/%m/%YE%H%M')))
    exitTimeMatches=re.findall(r'U\d{4}',times)
    for et in exitTimeMatches: timeStamps.append(ExitTimeStamp(datetime.datetime.strptime(date+et,'%d/%m/%YU%H%M')))
  return timeStamps

def parseAttFSFile(filename):
  with open(filename,'r') as f:
    CSVlines=f.readlines()
  giustificativi=[]
  for line in CSVlines:
    if line[0]=='#': continue
    cols=line.strip().split(',')
    startdt=datetime.datetime.strptime(cols[0],'%Y-%m-%d %H:%M')
    enddt=datetime.datetime.strptime(cols[1],'%Y-%m-%d %H:%M')
    text=','.join(cols[2:])
    giustificativi.append(AttivitaFuoriSede(startdt,enddt,text))
  for g in giustificativi:
    if not g.sanityCheck(): raise Exception('The following is not between 0 and 12 hours long, are you sure it\'s correct?:\n'+str(g))
  return giustificativi

def makePairs(timeStamps):
  timeStamps.sort(key=lambda ts:ts.timestamp)
  pairedTimeStamps=[]
  for k,g in itertools.groupby(timeStamps,key=lambda ts:ts.timestamp.day):
    tss=list(g)
    if len(tss)==2 and isinstance(tss[0],EntryTimeStamp) and isinstance(tss[1],ExitTimeStamp): pairedTimeStamps.append(PairTimeStamp(tss[0],tss[1]))
    else: pairedTimeStamps+=tss
  return pairedTimeStamps

if __name__=='__main__':

  parser=argparse.ArgumentParser()
  parser.add_argument('-s','--sissa')
  parser.add_argument('-a','--attivitaFS')
  parser.add_argument('-b','--browser',choices=['firefox','chrome'],default="firefox")

  args=parser.parse_args()

  if args.attivitaFS is not None:
    afs=parseAttFSFile(args.attivitaFS)
    print('*****************\nRead in the following attivita fuori sede giustificativi:\n'+'\n'.join([str(a) for a in afs])+'\n******************')
  if args.sissa is not None:
    timeStamps=parseFile(args.sissa)
    pairedTimeStamps=makePairs(timeStamps)
    print('Read in the following SISSA timestamps:\n'+'\n'.join([str(ts) for ts in pairedTimeStamps]))
  input("Press any key to open INFN webpage") 
  driver=None

  try:
    if args.browser=='chrome':
      driver = webdriver.Chrome()
    else:
      firefoxProfile = webdriver.FirefoxProfile()
      firefoxProfile.set_preference("browser.privatebrowsing.autostart", True)
      driver = webdriver.Firefox(firefox_profile=firefoxProfile)
    driver.get("https://presenze.dsi.infn.it/infnweb/")

    input("Press any key once logged in") 
    #editButton=driver.find_element_by_id('j_id148:14:j_id325:j_id326')
    #editButton.click()
    #table=driver.find_element_by_xpath("//table[@class='IceDatTbl']")
    if args.sissa is not None:
      force=False
      for ts in pairedTimeStamps:
        if not force: response=input("\nPress q to quit, n to skip this timestamp, a to input all, any other key to input timestamp "+str(ts)+"\n")
        else: print(ts)
        if response=='q': break
        elif response=='n': continue
        elif response=='a': force=True
        ts.enterData(driver)
        KeepTrying=True
        while KeepTrying:
            try: ts.enterData(driver)
            except WebDriverException as err:
                print(repr(err))
                response2=input("\nFAILED: press n to skip timestamp, any other key to try again")
                if response2=='n': KeepTrying=False
            else: KeepTrying=False
    if args.attivitaFS is not None:
      input("Press any key to open giustificativi box") 
      rgJavaLink=driver.find_element_by_link_text('Richiesta Giustificativo')
      rgJavaLink.click()
      force=False
      for a in afs:
        if not force: response=input("\nPress q to quit, n to skip this timestamp, a to input all, any other key to input giustificativo "+str(a)+"\n")
        else: print(a)
        if response=='q': break
        elif response=='n': continue
        elif response=='a': force=True
        KeepTrying=True
        while KeepTrying:
            try: a.enterData(driver)
            except WebDriverException as err:
                print(repr(err))
                response2=input("\nFAILED: press n to skip timestamp, any other key to try again")
                if response2=='n': KeepTrying=False
            else: KeepTrying=False
  finally:
    pass
    ##TODO:put me back in
    #if driver is not None: driver.close()


